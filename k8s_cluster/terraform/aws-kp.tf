# creating ssh-key.
resource "aws_key_pair" "project_kp" {
  key_name   = "project_kp"
  public_key = file("${path.module}/id_rsa.pub")
}
