resource "aws_instance" "jenkinsserver" {
  ami                    = var.image_id
  instance_type          = var.instance_type
  key_name               = aws_key_pair.project_kp.key_name
  iam_instance_profile   = aws_iam_instance_profile.test_profile.name
  vpc_security_group_ids = ["${aws_security_group.project_sg.id}"]
  tags = {
    Name = "jenkinsserver"
  }
  root_block_device {
    volume_size           = "30"
    volume_type           = "gp2"
    delete_on_termination = true
  }
  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("${path.module}/id_rsa")
    host        = self.public_ip
  }
  provisioner "file" {
    source      = "id_rsa.pub"                        # terraform machine
    destination = "/home/ubuntu/.ssh/authorized_keys" # remote machine
  }
}

resource "aws_instance" "sonarqubeserver" {
  ami                    = var.image_id
  instance_type          = var.instance_type
  key_name               = aws_key_pair.project_kp.key_name
  vpc_security_group_ids = ["${aws_security_group.project_sg.id}"]
  tags = {
    Name = "sonarqubeserver"
  }
  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("${path.module}/id_rsa")
    host        = self.public_ip
  }
  provisioner "file" {
    source      = "id_rsa.pub"                        # terraform machine
    destination = "/home/ubuntu/.ssh/authorized_keys" # remote machine
  }
}

resource "aws_instance" "elkserver" {
  ami                    = var.image_id
  instance_type          = var.instance_type
  key_name               = aws_key_pair.project_kp.key_name
  vpc_security_group_ids = ["${aws_security_group.project_sg.id}"]
  tags = {
    Name = "elkserver"
  }
  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("${path.module}/id_rsa")
    host        = self.public_ip
  }
  provisioner "file" {
    source      = "id_rsa.pub"                        # terraform machine
    destination = "/home/ubuntu/.ssh/authorized_keys" # remote machine
  }
}

resource "aws_instance" "k8smaster" {
  ami                    = var.image_id
  instance_type          = var.instance_type
  key_name               = aws_key_pair.project_kp.key_name
  vpc_security_group_ids = ["${aws_security_group.project_sg.id}"]
  tags = {
    Name = "k8smaster"
  }
  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("${path.module}/id_rsa")
    host        = self.public_ip
  }
  provisioner "file" {
    source      = "id_rsa.pub"                        # terraform machine
    destination = "/home/ubuntu/.ssh/authorized_keys" # remote machine
  }
}

resource "aws_instance" "k8sworker1" {
  ami                    = var.image_id
  instance_type          = var.instance_type
  key_name               = aws_key_pair.project_kp.key_name
  vpc_security_group_ids = ["${aws_security_group.project_sg.id}"]
  tags = {
    Name = "k8sworker1"
  }
  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("${path.module}/id_rsa")
    host        = self.public_ip
  }
  provisioner "file" {
    source      = "id_rsa.pub"                        # terraform machine
    destination = "/home/ubuntu/.ssh/authorized_keys" # remote machine
  }
}

resource "aws_instance" "k8sworker2" {
  ami                    = var.image_id
  instance_type          = var.instance_type
  key_name               = aws_key_pair.project_kp.key_name
  vpc_security_group_ids = ["${aws_security_group.project_sg.id}"]
  tags = {
    Name = "k8sworker2"
  }
  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("${path.module}/id_rsa")
    host        = self.public_ip
  }
  provisioner "file" {
    source      = "id_rsa.pub"                        # terraform machine
    destination = "/home/ubuntu/.ssh/authorized_keys" # remote machine
  }
}

