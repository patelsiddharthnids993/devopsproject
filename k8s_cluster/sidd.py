import subprocess
import time
import re
import argparse
import datetime

b2,b3,b4,b5,b6,b7 = '','','','','',''

def Get_Time():
        global b2,b3,b4,b5,b6,b7
        b1 = datetime.datetime.now()
        b2 = b1.strftime("%Y")
        b3 = b1.strftime("%m")
        b4 = b1.strftime("%d")
        b5 = b1.strftime("%H")
        b6 = b1.strftime("%M")
        b7 = b1.strftime("%S")

def countdown(t):
        while t:
                mins, secs = divmod(t, 60)
                timer = '{:02d}:{:02d}'.format(mins, secs)
                print(timer, end="\r")
                time.sleep(1)
                t -= 1

        print('Times up !!!')

def setupdp():
    print("************************************Setup Started*****************************************")
    print("============================================================================================")
    os_distribution = subprocess.check_output("grep -i ^id= /etc/os-release | sed 's/ID=//g'", shell=True, universal_newlines=True)

    if (os_distribution.strip()=='ubuntu'):
        print("You've UBUNTU OS")
        countdown(2)
        print("============================================================================================")

        print("Checking for key-pair in terraform repo")
        countdown(2)
        pub_key_check = subprocess.check_output("ls -1 terraform/id_rsa | wc -l", shell=True, universal_newlines=True)

        if (int(pub_key_check.strip()) == 0):
            print("Generating ssh keys for key pair to access AWS VMs and for ansible connectivity too")
            subprocess.check_output("ssh-keygen -q -t rsa -N '' -f terraform/id_rsa", shell=True, universal_newlines=True)

        elif (int(pub_key_check.strip()) == 1):
            print("You've SSH keys alreday")

        print("============================================================================================")
        print("Checking for terraform packages")
        countdown(2)
        terraform_package = subprocess.check_output("apt list --installed terraform 2>/dev/null | grep -i installed | wc -l", shell=True, universal_newlines=True)

        if (int(terraform_package.strip()) == 0):
            print("Package terraform isn't installed, installing it now !!!")
            countdown(2)
            subprocess.check_output("grep -A 6 terraform_package conf/configuration_ubuntu_setup.txt | grep -v terraform_package > conf/configuration_ubuntu_setup1.txt", shell=True, universal_newlines=True)

            file1 = open('conf/configuration_ubuntu_setup1.txt', 'r')
            Lines = file1.readlines()
            file1.close()

            for line in Lines:
                a1 = "{}".format(line.strip())
                subprocess.call(""+ a1 +"", shell=True, universal_newlines=True)
            subprocess.call("rm -rf conf/configuration_ubuntu_setup1.txt", shell=True, universal_newlines=True)

        else:
            print("You've Terraform installed")

        print("============================================================================================")
        print("Checking for AWS CLI packages")
        countdown(2)
        aws_package = subprocess.check_output("apt list --installed awscli 2>/dev/null | grep -i installed | wc -l", shell=True, universal_newlines=True)

        if (int(aws_package.strip()) == 0):
            print("Package AWS CLI isn't installed, installing it now !!!")
            countdown(2)
            subprocess.check_output("grep -A 2 aws_package conf/configuration_ubuntu_setup.txt | grep -v aws_package > conf/configuration_ubuntu_setup2.txt", shell=True, universal_newlines=True)

            file1 = open('conf/configuration_ubuntu_setup2.txt', 'r')
            Lines = file1.readlines()
            file1.close()

            for line in Lines:
                a1 = "{}".format(line.strip())
                subprocess.call(""+ a1 +"", shell=True, universal_newlines=True)
            subprocess.call("rm -rf conf/configuration_ubuntu_setup2.txt", shell=True, universal_newlines=True)


        else:
            print("You've AWS CLI installed")

        print("============================================================================================")
        print("Checking for Ansible packages")
        #countdown(2)
        ansible_package = subprocess.check_output("apt list --installed ansible 2>/dev/null | grep -i installed | wc -l", shell=True, universal_newlines=True)

        if (int(ansible_package.strip()) == 0):
            print("Package Ansible isn't installed, installing it now !!!")
            countdown(2)
            subprocess.check_output("grep -A 2 ansible_package conf/configuration_ubuntu_setup.txt | grep -v ansible_package > conf/configuration_ubuntu_setup3.txt", shell=True, universal_newlines=True)

            file1 = open('conf/configuration_ubuntu_setup3.txt', 'r')
            Lines = file1.readlines()
            file1.close()

            for line in Lines:
                a1 = "{}".format(line.strip())
                subprocess.call(""+ a1 +"", shell=True, universal_newlines=True)
            subprocess.call("rm -rf conf/configuration_ubuntu_setup3.txt", shell=True, universal_newlines=True)

        else:
            print("You've Ansible installed")
        print("============================================================================================")

        print("Checking for Trivy packages")
        countdown(2)
        trivy_package = subprocess.check_output("apt list --installed trivy 2>/dev/null | grep -i installed | wc -l", shell=True, universal_newlines=True)

        if (int(trivy_package.strip()) == 0):
            print("Package Trivy isn't installed, installing it now !!!")
            countdown(2)
            subprocess.check_output("grep -A 6 trivy_package conf/configuration_ubuntu_setup.txt | grep -v ansible_package > conf/configuration_ubuntu_setup4.txt", shell=True, universal_newlines=True)

            file1 = open('conf/configuration_ubuntu_setup4.txt', 'r')
            Lines = file1.readlines()
            file1.close()

            for line in Lines:
                a1 = "{}".format(line.strip())
                subprocess.call(""+ a1 +"", shell=True, universal_newlines=True)
            subprocess.call("rm -rf conf/configuration_ubuntu_setup4.txt", shell=True, universal_newlines=True)

        else:
            print("You've Trivy installed")
        print("============================================================================================")

        print("Initializing Terraform Repo.")
        subprocess.call("cd terraform; terraform init", shell=True, universal_newlines=True)


        print("============================================================================================")

        print("Setting up AWS CLI")
        countdown(2)

        aws_cli_setting = subprocess.check_output("ls -1 ~/.aws/config | wc -l", shell=True, universal_newlines=True)

        if (int(aws_cli_setting.strip()) == 0):
            subprocess.call("aws configure", shell=True, universal_newlines=True)

        else:
            print("AWS CLI got setup alreday")
        print("==================================================================================================")


        print("Checking for AWS S3 bucket named Delivery-Champion-mana-devsecops-2023-scanning-reports")
        countdown(2)

        aws_bucket_setting = subprocess.check_output("aws s3 ls | grep delivery-champion-mana-devsecops-2023-scanning-reports | wc -l", shell=True, universal_newlines=True)

        if (int(aws_bucket_setting.strip()) == 0):
            print("Creating the bucket")
            subprocess.call("aws s3 mb s3://delivery-champion-mana-devsecops-2023-scanning-reports", shell=True, universal_newlines=True)

        else:
            print("AWS S3 Bucket delivery-champion-mana-devsecops-2023-scanning-reports is already created")

        print("============================================================================================")

        print("************************************Setup Completed*****************************************")

def startdp():
    print("************************************Starting Installation*****************************************")
    print("==================================================================================================")

    ak1 = subprocess.check_output("cat ~/.aws/credentials | grep aws_access_key_id | sed 's/aws_access_key_id = //g'", shell=True, universal_newlines=True)
    ak = ak1.strip()

    sk1 = subprocess.check_output("cat ~/.aws/credentials | grep aws_secret_access_key | sed 's/aws_secret_access_key = //g'", shell=True, universal_newlines=True)
    sk = sk1.strip()
    countdown(2)

    subprocess.call("cp terraform/terraform.tfvars_orig terraform/terraform.tfvars", shell=True, universal_newlines=True)
    subprocess.call("sed -i 's/ACCESS_KEY/"+ ak +"/g' terraform/terraform.tfvars", shell=True, universal_newlines=True)
    subprocess.call("sed -i 's/SECRET_KEY/"+ sk +"/g' terraform/terraform.tfvars", shell=True, universal_newlines=True)
    print("==================================================================================================")

    print("Enter the openaiapi keys")
    openaiapi = str(input())
    countdown(2)
    subprocess.call("cp ansible/k8s/app.yml_orig ansible/k8s/app.yml", shell=True, universal_newlines=True)
    subprocess.call("sed -i 's/openaiapi/"+ openaiapi +"/g' ansible/k8s/app.yml", shell=True, universal_newlines=True)
    print("==================================================================================================")


    print("Launching the AWS VMs")
    countdown(2)
    subprocess.call("cd terraform; terraform apply -auto-approve", shell=True, universal_newlines=True)
    print("==================================================================================================")

    # PUB IP starts here
    print("Creating Ansible host file from default")
    countdown(2)
    subprocess.check_output("cp ansible/inventory/default_hosts ansible/inventory/hosts", shell=True)

    jenkinsserverPuIP = subprocess.check_output("aws ec2 describe-instances  --query \"Reservations[*].Instances[*].{PublicIP:PublicIpAddress}\" --filter \"Name=tag:Name,Values='jenkinsserver'\"", shell=True, universal_newlines=True)
    s0 = re.compile(r"\d+[.]\d+[.]\d+[.]\d+")
    m0 = s0.search(jenkinsserverPuIP)
    PuIPj = m0.group(0)

    print("Public IP of Jenkins Server",PuIPj)
    subprocess.check_output("sed -i 's/PuIPj/"+ PuIPj +"/g' ansible/inventory/hosts", shell=True, universal_newlines=True)

    sonarqubeserverPuIP = subprocess.check_output("aws ec2 describe-instances  --query \"Reservations[*].Instances[*].{PublicIP:PublicIpAddress}\" --filter \"Name=tag:Name,Values='sonarqubeserver'\"", shell=True, universal_newlines=True)
    s0 = re.compile(r"\d+[.]\d+[.]\d+[.]\d+")
    m0 = s0.search(sonarqubeserverPuIP)
    PuIPs = m0.group(0)

    print("Public IP of Sonarqube Server",PuIPs)
    subprocess.check_output("sed -i 's/PuIPs/"+ PuIPs +"/g' ansible/inventory/hosts", shell=True, universal_newlines=True)

    elkserverPuIP = subprocess.check_output("aws ec2 describe-instances  --query \"Reservations[*].Instances[*].{PublicIP:PublicIpAddress}\" --filter \"Name=tag:Name,Values='elkserver'\"", shell=True, universal_newlines=True)
    s0 = re.compile(r"\d+[.]\d+[.]\d+[.]\d+")
    m0 = s0.search(elkserverPuIP)
    PuIPelk = m0.group(0)

    print("Public IP of ELK Server",PuIPelk)
    a = subprocess.check_output("sed -i 's/PuIPelk/"+ PuIPelk +"/g' ansible/inventory/hosts", shell=True, universal_newlines=True)

    k8smasterPuIP = subprocess.check_output("aws ec2 describe-instances  --query \"Reservations[*].Instances[*].{PublicIP:PublicIpAddress}\" --filter \"Name=tag:Name,Values='k8smaster'\"", shell=True, universal_newlines=True)
    s0 = re.compile(r"\d+[.]\d+[.]\d+[.]\d+")
    m0 = s0.search(k8smasterPuIP)
    PuIPkm = m0.group(0)

    print("Public IP of K8s Master Server",PuIPkm)
    subprocess.check_output("sed -i 's/PuIPkm/"+ PuIPkm +"/g' ansible/inventory/hosts", shell=True, universal_newlines=True)

    k8sworker1PuIP = subprocess.check_output("aws ec2 describe-instances  --query \"Reservations[*].Instances[*].{PublicIP:PublicIpAddress}\" --filter \"Name=tag:Name,Values='k8sworker1'\"", shell=True, universal_newlines=True)
    s0 = re.compile(r"\d+[.]\d+[.]\d+[.]\d+")
    m0 = s0.search(k8sworker1PuIP)
    PuIPkw1 = m0.group(0)

    print("Public IP of K8s Worker1 Server",PuIPkw1)
    subprocess.check_output("sed -i 's/PuIPkw1/"+ PuIPkw1 +"/g' ansible/inventory/hosts", shell=True, universal_newlines=True)

    k8sworker2PuIP = subprocess.check_output("aws ec2 describe-instances  --query \"Reservations[*].Instances[*].{PublicIP:PublicIpAddress}\" --filter \"Name=tag:Name,Values='k8sworker2'\"", shell=True, universal_newlines=True)
    s0 = re.compile(r"\d+[.]\d+[.]\d+[.]\d+")
    m0 = s0.search(k8sworker2PuIP)
    PuIPkw2 = m0.group(0)

    print("Public IP of K8s Worker2 Server",PuIPkw2)
    subprocess.check_output("sed -i 's/PuIPkw2/"+ PuIPkw2 +"/g' ansible/inventory/hosts", shell=True, universal_newlines=True)

    final = subprocess.check_output("aws ec2 describe-instances  --query \"Reservations[*].Instances[*].{PublicIP:PublicIpAddress,PrivateIP:PrivateIpAddress,Name:Tags[?Key=='Name']|[0].Value,Status:State.Name}\" --filter \"Name=tag:Name,Values='jenkinsserver','sonarqubeserver','elkserver','k8smaster','k8sworker1','k8sworker2'\" \"Name=instance-state-name,Values=running\" --output table", shell=True, universal_newlines=True)
    print(final)
    print("==================================================================================================")

    print("Inserting Public and Private IP address of AWS VMs to the config files")
    countdown(2)

    subprocess.check_output("cp ansible/jenkins/conf/jenkinsnginx_nginx.conf_orig ansible/jenkins/conf/jenkinsnginx_nginx.conf", shell=True)
    subprocess.check_output("sed -i 's/PuIPj/"+ PuIPj +"/g' ansible/jenkins/conf/jenkinsnginx_nginx.conf", shell=True, universal_newlines=True)

    subprocess.check_output("cp ansible/sonarqube/conf/sonarqubenginx_nginx.conf_orig ansible/sonarqube/conf/sonarqubenginx_nginx.conf", shell=True)
    subprocess.check_output("sed -i 's/PuIPs/"+ PuIPs +"/g' ansible/sonarqube/conf/sonarqubenginx_nginx.conf", shell=True, universal_newlines=True)


    subprocess.check_output("cp ansible/elk_stack/elkserver.yml_orig ansible/elk_stack/elkserver.yml", shell=True, universal_newlines=True)
    subprocess.check_output("cp ansible/elk_stack/conf/filebeat.yml_orig ansible/elk_stack/conf/filebeat.yml", shell=True, universal_newlines=True)

    subprocess.check_output("cp ansible/elk_stack/conf/kibananginx_nginx.conf_orig ansible/elk_stack/conf/kibananginx_nginx.conf", shell=True, universal_newlines=True)
    subprocess.check_output("sed -i 's/PuIPelk/"+ PuIPelk +"/g' ansible/elk_stack/conf/kibananginx_nginx.conf", shell=True, universal_newlines=True)

    elkserverPrIP = subprocess.check_output("aws ec2 describe-instances  --query \"Reservations[*].Instances[*].{PrivateIP:PrivateIpAddress}\" --filter \"Name=tag:Name,Values='elkserver'\"", shell=True, universal_newlines=True)
    s0 = re.compile(r"\d+[.]\d+[.]\d+[.]\d+")
    m0 = s0.search(elkserverPrIP)
    PrIPelk = m0.group(0)

    subprocess.check_output("sed -i 's/PrIPelk/"+ PrIPelk +"/g' ansible/elk_stack/elkserver.yml", shell=True, universal_newlines=True)
    subprocess.check_output("sed -i 's/PrIPelk/"+ PrIPelk +"/g' ansible/elk_stack/conf/filebeat.yml", shell=True, universal_newlines=True)

    print("==================================================================================================")

    print("Creating .ssh dir and setting the permission to 700")
    countdown(2)
    subprocess.call("mkdir ~/.ssh && chmod 700 ~/.ssh", shell=True, universal_newlines=True)
    print("==================================================================================================")

    print("Coping the private key and ssh config file to .ssh dir")
    countdown(2)
    subprocess.call("cp -iv terraform/id_rsa ~/.ssh/id_rsa && cp -iv conf/ssh_config.txt /home/ubuntu/.ssh/config", shell=True, universal_newlines=True)
    print("==================================================================================================")

    print("Generating the ssh config file")
    countdown(2)
    subprocess.check_output("sed -i 's/PuIPj/"+ PuIPj +"/g' /home/ubuntu/.ssh/config", shell=True, universal_newlines=True)
    subprocess.check_output("sed -i 's/PuIPs/"+ PuIPs +"/g' /home/ubuntu/.ssh/config", shell=True, universal_newlines=True)
    subprocess.check_output("sed -i 's/PuIPelk/"+ PuIPelk +"/g' /home/ubuntu/.ssh/config", shell=True, universal_newlines=True)
    subprocess.check_output("sed -i 's/PuIPkm/"+ PuIPkm +"/g' /home/ubuntu/.ssh/config", shell=True, universal_newlines=True)
    subprocess.check_output("sed -i 's/PuIPkw1/"+ PuIPkw1 +"/g' /home/ubuntu/.ssh/config", shell=True, universal_newlines=True)
    subprocess.check_output("sed -i 's/PuIPkw2/"+ PuIPkw2 +"/g' /home/ubuntu/.ssh/config", shell=True, universal_newlines=True)
    print("==================================================================================================")

    print("Installing Kubernetes Cluster using Ansible")
    countdown(2)
    subprocess.call("cd ansible/k8s; ansible-playbook first.yml -i ../inventory/hosts", shell=True, universal_newlines=True)
    subprocess.call("cd ansible/k8s; ansible-playbook second.yml -i ../inventory/hosts", shell=True, universal_newlines=True)
    subprocess.call("cd ansible/k8s; ansible-playbook third.yml -i ../inventory/hosts", shell=True, universal_newlines=True)
    print("==================================================================================================")

    print("Setting up ssh permission for ubuntu user on K8s master node")
    countdown(2)
    subprocess.call("cd ansible/k8s; ansible-playbook test.yml -i ../inventory/hosts", shell=True, universal_newlines=True)
    print("==================================================================================================")

    print("Deploying Application on K8s cluster")
    countdown(2)
    subprocess.call("cd ansible/k8s; ansible-playbook app.yml -i ../inventory/hosts", shell=True, universal_newlines=True)
    print("==================================================================================================")

    print("Deploying Prometheus Stack on K8s cluster")
    countdown(2)
    subprocess.call("cd ansible/k8s;ansible-playbook prometheus_stack.yml -i ../inventory/hosts", shell=True, universal_newlines=True)
    print("==================================================================================================")

    print("Installing Jenkins on Jenkinsserver using Ansible")
    countdown(2)
    subprocess.call("cd ansible/jenkins; ansible-playbook jenkins.yml -i ../inventory/hosts", shell=True, universal_newlines=True)
    print("==================================================================================================")

    print("Generating the ssh config file")
    countdown(2)
    subprocess.check_output("cp ansible/jenkins/conf/ssh_config.txt_orig ansible/jenkins/conf/ssh_config.txt", shell=True, universal_newlines=True)
    subprocess.check_output("sed -i 's/PuIPkm/"+ PuIPkm +"/g' ansible/jenkins/conf/ssh_config.txt", shell=True, universal_newlines=True)
    print("==================================================================================================")

    print("Setting up ssh permission for Jenkins user on Jenkins node")
    countdown(2)
    subprocess.call("cd ansible/jenkins; ansible-playbook test.yml -i ../inventory/hosts", shell=True, universal_newlines=True)
    print("==================================================================================================")

    print("Installing Aqua Trivy on Jenkins server")
    countdown(2)
    subprocess.call("cd ansible/trivy; ansible-playbook trivy.yml -i ../inventory/hosts", shell=True, universal_newlines=True)
    print("==================================================================================================")

    print("Installing Sonarqube on sonarqubeserver using Ansible")
    countdown(2)
    subprocess.call("cd ansible/sonarqube; ansible-playbook sonarqube.yml -i ../inventory/hosts", shell=True, universal_newlines=True)
    print("==================================================================================================")

    print("Installing Sonarqube Scanner on jenkins server using Ansible")
    countdown(2)
    subprocess.call("cd ansible/sonarqube; ansible-playbook sonarqube_scanner.yml -i ../inventory/hosts", shell=True, universal_newlines=True)
    print("==================================================================================================")

    print("Installing ELK Stack on elkserver using Ansible")
    countdown(2)
    subprocess.call("cd ansible/elk_stack; ansible-playbook elkserver.yml -i ../inventory/hosts", shell=True, universal_newlines=True)
    print("==================================================================================================")

    print("Getting the Jenkins initial Admin Password")
    countdown(3)
    jen_pass = subprocess.check_output("ssh -i terraform/id_rsa ubuntu@"+ PuIPj +" sudo cat /var/lib/jenkins/secrets/initialAdminPassword", shell=True, universal_newlines=True)
    print("Login to Jenkins web GUI and paste this password => ",jen_pass)
    print("==================================================================================================")

    print("************************************Installation Completed*****************************************")

def scancheck():
    print("************************************Scan Started********************************************")
    print("==================================================================================================")
    countdown(3)
    print("Formatting the terraform code")
    subprocess.call("cd terraform && terraform fmt", shell=True, universal_newlines=True)
    print("==================================================================================================")
    print("==================================================================================================")
    countdown(3)
    print("Validating the terraform code against misconfiguration")
    subprocess.call("cd terraform && terraform validate", shell=True, universal_newlines=True)
    print("==================================================================================================")
    print("==================================================================================================")
    countdown(3)
    Get_Time()
    print("Scanning the terraform code")
    trivy_details = subprocess.check_output("ls -ltr | grep trivy_report | wc -l", shell=True, universal_newlines=True)
    if (int(trivy_details.strip()) == 1):
        subprocess.call("trivy config terraform/ --format template --template \"@/usr/local/share/trivy/templates/html.tpl\" -o trivy_report/trivy-infrastructure-scanning-report_"+ b2 +"_"+ b3 +"_"+ b4 +"_"+ b5 +"_"+ b6 +"_"+ b7 +".html", shell=True, universal_newlines=True)
        subprocess.call("aws s3 sync trivy_report/ s3://delivery-champion-mana-devsecops-2023-scanning-reports", shell=True, universal_newlines=True)
        print("==================================================================================================")
    else:
        subprocess.check_output("mkdir trivy_report", shell=True, universal_newlines=True)
        subprocess.call("trivy config terraform/ --format template --template \"@/usr/local/share/trivy/templates/html.tpl\" -o trivy_report/trivy-infrastructure-scanning-report_"+ b2 +"_"+ b3 +"_"+ b4 +"_"+ b5 +"_"+ b6 +"_"+ b7 +".html", shell=True, universal_newlines=True)
        print("Uploading the report to AWS S3 bucket delivery-champion-mana-devsecops-2023-scanning-reports")
        subprocess.call("aws s3 sync trivy_report/ s3://delivery-champion-mana-devsecops-2023-scanning-reports", shell=True, universal_newlines=True)
        print("==================================================================================================")

def stopdp():

    print("************************************Destruction Started********************************************")
    print("==================================================================================================")
    print("Destroying the AWS server")
    countdown(2)
    subprocess.call("cd terraform; terraform destroy -auto-approve", shell=True, universal_newlines=True)
    print("==================================================================================================")
    print("Deleting the AWS S3 bucket delivery-champion-mana-devsecops-2023-scanning-reports")
    countdown(2)
    subprocess.call("aws s3 rb s3://delivery-champion-mana-devsecops-2023-scanning-reports --force", shell=True, universal_newlines=True)
    print("==================================================================================================")

    print("Removing files")
    countdown(2)
    subprocess.check_output("rm -rf ansible/inventory/hosts", shell=True, universal_newlines=True)
    subprocess.check_output("rm -rf ansible/webserver/conf/learneasyai_nginx.conf", shell=True, universal_newlines=True)
    subprocess.check_output("rm -rf ansible/elk_stack/elkserver.yml", shell=True, universal_newlines=True)
    subprocess.check_output("rm -rf ansible/elk_stack/conf/filebeat.yml", shell=True, universal_newlines=True)
    subprocess.check_output("rm -rf ansible/prometheus_stack/conf/node-rules.yml", shell=True, universal_newlines=True)
    subprocess.check_output("rm -rf ansible/prometheus_stack/conf/prometheus.yml", shell=True, universal_newlines=True)
    subprocess.check_output("rm -rf ansible/sonarqube/conf/sonarqubenginx_nginx.conf", shell=True, universal_newlines=True)
    subprocess.check_output("rm -rf ansible/jenkins/conf/jenkinsnginx_nginx.conf", shell=True, universal_newlines=True)
    subprocess.check_output("rm -rf rm -rf terraform/terraform.tfvars", shell=True, universal_newlines=True)
    subprocess.check_output("rm -rf terraform/terraform.tfstate", shell=True, universal_newlines=True)
    subprocess.check_output("rm -rf terraform/terraform.tfstate.backup", shell=True, universal_newlines=True)
    subprocess.check_output("rm -rf terraform/.terraform*", shell=True, universal_newlines=True)
    subprocess.check_output("rm -rf terraform/id_rsa*", shell=True, universal_newlines=True)
    subprocess.check_output("rm -rf ~/.ssh", shell=True, universal_newlines=True)
    subprocess.check_output("rm -rf ansible/jenkins/conf/ssh_config.txt", shell=True, universal_newlines=True)
    subprocess.check_output("rm -rf ansible/k8s/app.yml", shell=True, universal_newlines=True)
    subprocess.check_output("rm -rf trivy_report/*", shell=True, universal_newlines=True)
    subprocess.check_output("rm -rf ansible/elk_stack/conf/kibananginx_nginx.conf", shell=True, universal_newlines=True)
    print("==================================================================================================")
    print("************************************Destruction Completed********************************************")

if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-A', '--action', dest='action', help='Action you would like to do', type=str)
    args = parser.parse_args()

    action = args.action

    if (action=='setup'):
       setupdp()
    elif (action=='start'):
        startdp()
    elif (action=='stop'):
        stopdp()
    elif (action=='status'):
        statusad()
    elif (action=='port_check'):
        port_check()
    elif (action=='conn_check'):
        conncheck()
    elif (action=='scan'):
        scancheck()
    else:
        print("python3 automation_devops.py -A <setup|start|stop|status|scan>")

