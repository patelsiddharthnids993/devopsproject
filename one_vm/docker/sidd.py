import subprocess
import re
import time
import argparse
import datetime

def countdown(t):
        while t:
                mins, secs = divmod(t, 60)
                timer = '{:02d}:{:02d}'.format(mins, secs)
                print(timer, end="\r")
                time.sleep(1)
                t -= 1

        print('Times up !!!')

def setupad():
    print("***************Started*********************")
    os_distribution = subprocess.check_output("grep -i ^id= /etc/os-release | sed 's/ID=//g'", shell=True, universal_newlines=True)

    if (os_distribution.strip()=='ubuntu'):
        print("You've UBUNTU OS")
        countdown(2)

        print("============================================================================================")
        print("Checking for Docker packages")
        countdown(2)
        docker_packages = subprocess.check_output("apt list --installed docker-ce 2>/dev/null | grep -i installed | wc -l", shell=True, universal_newlines=True)

        if (int(docker_packages.strip()) == 0):
            print("Package Docker isn't installed, installing it now !!!")
            countdown(2)
            subprocess.check_output("grep -A 6 docker_package conf/configuration_ubuntu_setup.txt | grep -v docker_package > conf/configuration_ubuntu_setup4.txt", shell=True, universal_newlines=True)

            file1 = open('conf/configuration_ubuntu_setup4.txt', 'r')
            Lines = file1.readlines()
            file1.close()

            for line in Lines:
                a1 = "{}".format(line.strip())
                subprocess.call(""+ a1 +"", shell=True, universal_newlines=True)
            subprocess.call("rm -rf conf/configuration_ubuntu_setup4.txt", shell=True, universal_newlines=True)

        else:
            print("You've Docker installed")

        print("============================================================================================")
        print("Checking for Docker-Compose packages")
        countdown(2)
        dc_packages = subprocess.check_output("ls -1 /usr/local/bin/docker-compose | wc -l", shell=True, universal_newlines=True)

        if (int(dc_packages.strip()) == 0):
            print("Package Docker-compose isn't installed, installing it now !!!")
            countdown(2)
            subprocess.check_output("grep -A 3 docker-compose_package conf/configuration_ubuntu_setup.txt | grep -v docker-compose_package > conf/configuration_ubuntu_setup5.txt", shell=True, universal_newlines=True)

            file1 = open('conf/configuration_ubuntu_setup5.txt', 'r')
            Lines = file1.readlines()
            file1.close()

            for line in Lines:
                a1 = "{}".format(line.strip())
                subprocess.call(""+ a1 +"", shell=True, universal_newlines=True)
            subprocess.call("rm -rf conf/configuration_ubuntu_setup5.txt", shell=True, universal_newlines=True)

        else:
            print("You've Docker-Compose installed")


    print("**************Completed the Work****************")


def startad():
    print("***************Started*********************")
    print("Getting Docker images and making containers up and running")
    countdown(3)
    subprocess.call("sudo docker-compose up -d", shell=True, universal_newlines=True)
    print("*******************************************")

    print("Generating SSH Key Pair to communicate to Ansible node")
    countdown(3)
    subprocess.call("ssh-keygen -q -t rsa -N '' -f id_rsa", shell=True, universal_newlines=True)
    print("*******************************************")

    print("Copying SSH Key Pair to all the containers")
    countdown(3)
    subprocess.call("sudo docker cp id_rsa.pub jenkinsserver:/home/ubuntu/.ssh/authorized_keys", shell=True, universal_newlines=True)
    subprocess.call("sudo docker cp id_rsa.pub webserver:/home/ubuntu/.ssh/authorized_keys", shell=True, universal_newlines=True)
    subprocess.call("sudo docker cp id_rsa.pub elkserver:/home/ubuntu/.ssh/authorized_keys", shell=True, universal_newlines=True)
    subprocess.call("sudo docker cp id_rsa.pub pgaserver:/home/ubuntu/.ssh/authorized_keys", shell=True, universal_newlines=True)
    print("*******************************************")

    print("Fixing permission on all the containers")
    countdown(3)
    subprocess.call("sudo docker exec -it jenkinsserver sudo chown ubuntu:ubuntu /home/ubuntu/.ssh/authorized_keys", shell=True, universal_newlines=True)
    subprocess.call("sudo docker exec -it webserver sudo chown ubuntu:ubuntu /home/ubuntu/.ssh/authorized_keys", shell=True, universal_newlines=True)
    subprocess.call("sudo docker exec -it elkserver sudo chown ubuntu:ubuntu /home/ubuntu/.ssh/authorized_keys", shell=True, universal_newlines=True)
    subprocess.call("sudo docker exec -it pgaserver sudo chown ubuntu:ubuntu /home/ubuntu/.ssh/authorized_keys", shell=True, universal_newlines=True)
    print("*******************************************")

    print("Setting up the ansible hosts file with the IP address")
    countdown(3)

    subprocess.check_output("cp ansible/inventory/default_hosts ansible/inventory/hosts", shell=True, universal_newlines=True)

    j_ip = subprocess.check_output("sudo docker network inspect docker_ci-cd_network | grep -A 4 jenkinsserver | grep IPv4Address", shell=True, universal_newlines=True)

    s1 = re.compile(r'[0-9.]+/')
    m1 = s1.search(j_ip)
    jip = (m1.group(0).rstrip('/'))
    print(jip)

    w_ip = subprocess.check_output("sudo docker network inspect docker_ci-cd_network | grep -A 4 webserver | grep IPv4Address", shell=True, universal_newlines=True)

    s1 = re.compile(r'[0-9.]+/')
    m1 = s1.search(w_ip)
    wip = (m1.group(0).rstrip('/'))
    print(wip)

    elk_ip = subprocess.check_output("sudo docker network inspect docker_ci-cd_network | grep -A 4 elkserver | grep IPv4Address", shell=True, universal_newlines=True)

    s1 = re.compile(r'[0-9.]+/')
    m1 = s1.search(elk_ip)
    elkip = (m1.group(0).rstrip('/'))
    print(elkip)

    pga_ip = subprocess.check_output("sudo docker network inspect docker_ci-cd_network | grep -A 4 pgaserver | grep IPv4Address", shell=True, universal_newlines=True)

    s1 = re.compile(r'[0-9.]+/')
    m1 = s1.search(pga_ip)
    pgaip = (m1.group(0).rstrip('/'))
    print(pgaip)

    subprocess.check_output("sed -i 's/jip/"+ jip +"/g' ansible/inventory/hosts", shell=True, universal_newlines=True)
    subprocess.check_output("sed -i 's/wip/"+ wip +"/g' ansible/inventory/hosts", shell=True, universal_newlines=True)
    subprocess.check_output("sed -i 's/elkip/"+ elkip +"/g' ansible/inventory/hosts", shell=True, universal_newlines=True)
    subprocess.check_output("sed -i 's/pgaip/"+ pgaip +"/g' ansible/inventory/hosts", shell=True, universal_newlines=True)

    print("*******************************************")

    print("Deplying Jenkins using Ansible Playbook")
    countdown(3)

    subprocess.call("cd ansible/jenkins && ansible-playbook -i ../inventory/hosts jenkins.yml", shell=True, universal_newlines=True)
    print("*******************************************")

    print("Deplying Sonarqube-Scanner on Jenkinsserver using Ansible Playbook")
    countdown(3)

    subprocess.call("cd ansible/sonarqube && ansible-playbook -i ../inventory/hosts sonarqube_scanner.yml", shell=True, universal_newlines=True)
    print("*******************************************")


    print("Deplying the Django application  on Web Container using Playbook")
    countdown(3)
    subprocess.call("cd ansible/webserver && ansible-playbook django.yml -i ../inventory/hosts", shell=True, universal_newlines=True)
    print("*******************************************")

    print("Deplying the ELK stack using Ansible Playbook")
    countdown(3)
    subprocess.call("cd ansible/elk_stack && ansible-playbook elkserver.yml -i ../inventory/hosts", shell=True, universal_newlines=True)
    print("*******************************************")

    print("Deplying the Prometheus stack using Ansible Playbook")
    countdown(3)
    subprocess.call("cd ansible/prometheus_stack && ansible-playbook pgaserver.yml -i ../inventory/hosts", shell=True, universal_newlines=True)
    print("*******************************************")


    print("Getting the Jenkins initial Admin Password")
    countdown(3)
    jen_pass = subprocess.check_output("sudo docker exec -it jenkinsserver sudo cat /var/lib/jenkins/secrets/initialAdminPassword", shell=True, universal_newlines=True)
    print("Login to Jenkins web GUI and paste this password => ",jen_pass)
    print("*******************************************")

    '''print("printing the jenkinsfile")
    countdown(3)
    #web_ip1 = subprocess.check_output("docker network inspect docker-compose_ci_cd_1_network | grep -A 4 webserver | grep IPv4Address", shell=True, universal_newlines=True)

    #s1 = re.compile(r'[0-9.]+/')
    #m1 = s1.search(web_ip1)
    #web_ip = (m1.group(0).rstrip('/'))

    #subprocess.check_output("cp Jenkinsfile Jenkinsfile.bkp", shell=True, universal_newlines=True)
    #subprocess.check_output("sed -i 's/changeit/"+ web_ip +"/g' Jenkinsfile", shell=True, universal_newlines=True)
    #subprocess.check_output("sed -i 's/changeit/webserver/g' Jenkinsfile", shell=True, universal_newlines=True)
    print("Use the Jenkinsfile mentioned below for Pipeline")
    print("*******************************************")
    subprocess.call("cat Jenkinsfile", shell=True, universal_newlines=True)
    #subprocess.check_output("cp Jenkinsfile.bkp Jenkinsfile", shell=True, universal_newlines=True)
    #subprocess.check_output("rm -rf Jenkinsfile.bkp", shell=True, universal_newlines=True)

    print("*******************************************")'''

def stopad():
    print("Stopping and removing containers, images, network")
    countdown(3)
    subprocess.call("sudo docker-compose down --rmi all", shell=True, universal_newlines=True)
    print("*******************************************")

    print("Deleting the files")
    subprocess.call("rm -rf id_rsa*", shell=True, universal_newlines=True)
    subprocess.call("rm -rf ansible/inventory/hosts", shell=True, universal_newlines=True)
    print("*******************************************")

def hardstopad():
    print ("==============================================================================")

    print("Checking for containers")
    a = subprocess.check_output("sudo docker ps -a | grep -v NAME | wc -l", shell=True, universal_newlines=True)
    if (int(a.strip()) == 0):
        print("No Containers running")
    elif (int(a.strip()) != 0):
        print ("One or more containers running, killing all containers")
        countdown(2)
        subprocess.call("sudo docker ps -a | grep -v NAMES | awk {'print $1'} | xargs sudo docker rm -f", shell=True, universal_newlines=True)
    print ("==============================================================================")

    print("Checking for images")
    b = subprocess.check_output("sudo docker images | grep -v SIZE | wc -l", shell=True, universal_newlines=True)
    if (int(b.strip()) == 0):
         print("No images")
    elif (int(b.strip()) != 0):
        print ("Killing all images")
        countdown(2)
        subprocess.call("sudo docker images | grep -v SIZE | awk {'print $3'} | xargs sudo docker rmi -f", shell=True, universal_newlines=True)
    print ("==============================================================================")

    print("Checking for volumes")
    c = subprocess.check_output("sudo docker volume ls | grep -v NAME | wc -l", shell=True, universal_newlines=True)
    if (int(c.strip()) == 0):
         print("No Volumes")
    elif (int(c.strip()) != 0):
        print ("Killing all volumes")
        countdown(2)
        subprocess.call("sudo docker volume ls | grep -v NAME | awk {'print $2'} | xargs sudo docker volume rm", shell=True, universal_newlines=True)
    print ("==============================================================================")

    print ("Checking for networks")
    d = subprocess.check_output("sudo docker network ls | grep -v NAME | awk {'print $2'} | grep -v bridge | grep -v none | grep -v host | wc -l", shell=True, universal_newlines=True)
    if (int(d.strip()) == 0):
         print("No Networks")
    elif (int(d.strip()) != 0):
        print("Killing the networks")
        countdown(2)
        subprocess.call("sudo docker network ls | grep -v NAME | awk {'print $2'} | grep -v bridge | grep -v none | grep -v host | xargs sudo docker network rm", shell=True, universal_newlines=True)
    print ("==============================================================================")

def statusad():

    print ("==============================================================================")
    a  = subprocess.check_output("docker ps -a | grep -v NAME | wc -l", shell=True, universal_newlines=True)
    if (int(a.strip()) == 0):
         print("No Containers exists")
    elif (int(a.strip()) != 0):
        print(a.strip(), " Containers found, please find the result mentioned below")
        print ("Details about containers")
        for i in range(int(a.strip())):
            j = str(i + 1)
            docker_id = subprocess.check_output("docker ps -a --format '{{.ID}}' | head -"+ j +" | tail -1", shell=True, universal_newlines=True)
            docker_name = subprocess.check_output("docker ps -a --format '{{.Names}}' | head -"+ j +" | tail -1", shell=True, universal_newlines=True)
            docker_image = subprocess.check_output("docker ps -a --format '{{.Image}}' | head -"+ j +" | tail -1", shell=True, universal_newlines=True)
            docker_status = subprocess.check_output("docker ps -a --format '{{.Status}}' | head -"+ j +" | tail -1", shell=True, universal_newlines=True)
            docker_size = subprocess.check_output("docker ps -a --format '{{.Size}}' | head -"+ j +" | tail -1", shell=True, universal_newlines=True)

            print(" ID => ",docker_id.strip(),","" NAME => ",docker_name.strip(),","" IMAGE =>", docker_image.strip(),","" STATUS => ", docker_status.strip(),","" SIZE => ", docker_size.strip())
    print ("==============================================================================")

    b = subprocess.check_output("docker images | grep -v SIZE | wc -l", shell=True, universal_newlines=True)
    if (int(b.strip()) == 0):
         print("No images")
    elif (int(b.strip()) != 0):
        print (b.strip()," Images present in this box, please find the result mentioned below")
        for i in range(int(b.strip())):
            j = str(i + 1)
            docker_image = subprocess.check_output("docker images | grep -v SIZE | awk {'print $1\":\"$2'} | head -"+ j +" | tail -1", shell=True, universal_newlines=True)
            docker_size = subprocess.check_output("docker images  --format '{{.Size}}' | head -"+ j +" | tail -1", shell=True, universal_newlines=True)
            print("Docker Image => ",docker_image.strip()," Docker Size => ",docker_size.strip())
    print ("==============================================================================")

    d = subprocess.check_output("docker volume ls | grep -v NAME | wc -l", shell=True, universal_newlines=True)
    if (int(d.strip()) == 0):
         print("No Volumes")
    elif (int(d.strip()) != 0):
        print ("Volumes present in this box")
        subprocess.call("docker volume ls | grep -v NAME | awk {'print $2'}", shell=True, universal_newlines=True)
    print ("==============================================================================")

    e = subprocess.check_output("docker network ls | grep -v NAME | awk {'print $2'} | grep -v bridge | grep -v none | grep -v host | wc -l", shell=True, universal_newlines=True)
    if (int(e.strip()) == 0):
         print("No Networks")
    elif (int(e.strip()) != 0):
        print ("Networks present in this box")
        subprocess.call("docker network ls | grep -v NAME | awk {'print $2'} | grep -v bridge | grep -v none | grep -v host", shell=True, universal_newlines=True)
    print ("==============================================================================")

def port_check():
    # Getting result from container
    count = subprocess.check_output("sudo docker ps --format '{{.Names}}' | wc -l", shell=True, universal_newlines=True)

    for i in range(int(count.strip())):
        j = str(i + 1)
        #print("==========================================================================================================")
        docker_name = subprocess.check_output("sudo docker ps --format '{{.Names}}' | head -"+ j +" | tail -1", shell=True, universal_newlines=True)
        #print(docker_name.strip())
        port_command = subprocess.check_output("sudo docker exec -it "+ docker_name.strip() +" netstat -ltnp | grep -v Active | grep -v Proto", shell=True, universal_newlines=True)
        #print(port_command.strip())
        #print("==========================================================================================================")

        #Get_Time
        b1 = datetime.datetime.now()
        b2 = b1.strftime("%X")
        b3 = b1.strftime("%x")
        b4 = b1.strftime("%Y")
        b5 = b1.strftime("%m")
        b6 = b1.strftime("%d")
        b7 = b1.strftime("%H")
        b8 = b1.strftime("%M")
        b9 = b1.strftime("%S")

        # File_Write():
        f = open("logs/port_ad.log", "a+")
        f.write("==========================================================================================================\n")
        f.write(""+ b3 +" "+ b2 +" "+ docker_name +"")
        f.write(""+ port_command +"")
        f.write("==========================================================================================================\n")
        f.close()

        # Log_Mng():
        c1 = subprocess.check_output("ls -ltr logs/port_ad.log | awk '{print $5}'", shell=True)
        if (int(c1) >= 20000):
                subprocess.check_output("mv logs/port_ad.log logs/port_ad.log"+b4+""+b5+""+b6+""+b7+""+b8+""+b9+"", shell=True)
        c2 = subprocess.check_output("find logs/port_ad.log* -mtime +7 -exec rm -rf {} \;", shell=True)

def conncheck():
    print("Checking the connectivity to all the containers from Ansible container")
    countdown(3)
    subprocess.call("docker exec -it ansibleserver /bin/bash -c 'cd prod_test/ansible_host/script && python3 SaveCompare.py -con connect -type mc'", shell=True, universal_newlines=True)
    print("*******************************************")

if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-A', '--action', dest='action', help='Action you would like to do', type=str)
    args = parser.parse_args()

    action = args.action

    if (action=='setup'):
       setupad()
    elif (action=='start'):
        startad()
    elif (action=='stop'):
        stopad()
    elif (action=='status'):
        statusad()
    elif (action=='port_check'):
        port_check()
    elif (action=='hard_stop'):
        hardstopad()
    elif (action=='conn_check'):
        conncheck()
    else:
        print("python3 automation_devops.py -A <setup|start|stop|hard_stop|status|port_check|conn_check>")


    print("***************Completed*********************")
