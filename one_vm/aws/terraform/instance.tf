resource "aws_instance" "jenkinsserver" {
  ami                    = var.image_id
  instance_type          = var.instance_type
  key_name               = aws_key_pair.project_kp.key_name
  vpc_security_group_ids = ["${aws_security_group.project_sg.id}"]
  tags = {
    Name = "jenkinsserver"
  }
  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("${path.module}/id_rsa")
    host        = self.public_ip
  }
  provisioner "file" {
    source      = "id_rsa.pub"      # terraform machine
    destination = "/home/ubuntu/.ssh/authorized_keys" # remote machine
  }
}

resource "aws_instance" "sonarqubeserver" {
  ami                    = var.image_id
  instance_type          = var.instance_type
  key_name               = aws_key_pair.project_kp.key_name
  vpc_security_group_ids = ["${aws_security_group.project_sg.id}"]
  tags = {
    Name = "sonarqubeserver"
  }
  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("${path.module}/id_rsa")
    host        = self.public_ip
  }
  provisioner "file" {
    source      = "id_rsa.pub"      # terraform machine
    destination = "/home/ubuntu/.ssh/authorized_keys" # remote machine
  }
}


resource "aws_instance" "webserver" {
  ami                    = var.image_id
  instance_type          = var.instance_type
  key_name               = aws_key_pair.project_kp.key_name
  vpc_security_group_ids = ["${aws_security_group.project_sg.id}"]
  tags = {
    Name = "webserver"
  }
  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("${path.module}/id_rsa")
    host        = self.public_ip
  }
  provisioner "file" {
    source      = "id_rsa.pub"      # terraform machine
    destination = "/home/ubuntu/.ssh/authorized_keys" # remote machine
  }
}

resource "aws_instance" "elkserver" {
  ami                    = var.image_id
  instance_type          = var.instance_type
  key_name               = aws_key_pair.project_kp.key_name
  vpc_security_group_ids = ["${aws_security_group.project_sg.id}"]
  tags = {
    Name = "elkserver"
  }
  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("${path.module}/id_rsa")
    host        = self.public_ip
  }
  provisioner "file" {
    source      = "id_rsa.pub"      # terraform machine
    destination = "/home/ubuntu/.ssh/authorized_keys" # remote machine
  }
}

resource "aws_instance" "pgaserver" {
  ami                    = var.image_id
  instance_type          = var.instance_type
  key_name               = aws_key_pair.project_kp.key_name
  vpc_security_group_ids = ["${aws_security_group.project_sg.id}"]
  tags = {
    Name = "pgaserver"
  }
  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("${path.module}/id_rsa")
    host        = self.public_ip
  }
  provisioner "file" {
    source      = "id_rsa.pub"      # terraform machine
    destination = "/home/ubuntu/.ssh/authorized_keys" # remote machine
  }
}

