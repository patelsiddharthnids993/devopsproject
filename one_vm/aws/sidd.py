import subprocess
import time
import re
import argparse
import datetime

def countdown(t):
        while t:
                mins, secs = divmod(t, 60)
                timer = '{:02d}:{:02d}'.format(mins, secs)
                print(timer, end="\r")
                time.sleep(1)
                t -= 1

        print('Times up !!!')

def setupdp():
    print("************************************Setup Started*****************************************")
    print("============================================================================================")
    os_distribution = subprocess.check_output("grep -i ^id= /etc/os-release | sed 's/ID=//g'", shell=True, universal_newlines=True)

    if (os_distribution.strip()=='ubuntu'):
        print("You've UBUNTU OS")
        countdown(2)
        print("============================================================================================")

        print("Checking for key-pair in terraform repo")
        countdown(2)
        pub_key_check = subprocess.check_output("ls -1 terraform/id_rsa | wc -l", shell=True, universal_newlines=True)
        
        if (int(pub_key_check.strip()) == 0):
            print("Generating ssh keys for key pair to access AWS VMs and for ansible connectivity too")
            subprocess.check_output("ssh-keygen -q -t rsa -N '' -f terraform/id_rsa", shell=True, universal_newlines=True)

        elif (int(pub_key_check.strip()) == 1):
            print("You've SSH keys alreday")

        print("============================================================================================")
        print("Checking for terraform packages")
        countdown(2)
        terraform_package = subprocess.check_output("apt list --installed terraform 2>/dev/null | grep -i installed | wc -l", shell=True, universal_newlines=True)

        if (int(terraform_package.strip()) == 0):
            print("Package terraform isn't installed, installing it now !!!")
            countdown(2)
            subprocess.check_output("grep -A 6 terraform_package conf/configuration_ubuntu_setup.txt | grep -v terraform_package > conf/configuration_ubuntu_setup1.txt", shell=True, universal_newlines=True)

            file1 = open('conf/configuration_ubuntu_setup1.txt', 'r')
            Lines = file1.readlines()
            file1.close()

            for line in Lines:
                a1 = "{}".format(line.strip())
                subprocess.call(""+ a1 +"", shell=True, universal_newlines=True)
            subprocess.call("rm -rf conf/configuration_ubuntu_setup1.txt", shell=True, universal_newlines=True)

        else:
            print("You've Terraform installed")

        print("============================================================================================")
        print("Checking for AWS CLI packages")
        countdown(2)
        aws_package = subprocess.check_output("apt list --installed awscli 2>/dev/null | grep -i installed | wc -l", shell=True, universal_newlines=True)

        if (int(aws_package.strip()) == 0):
            print("Package AWS CLI isn't installed, installing it now !!!")
            countdown(2)
            subprocess.check_output("grep -A 2 aws_package conf/configuration_ubuntu_setup.txt | grep -v aws_package > conf/configuration_ubuntu_setup2.txt", shell=True, universal_newlines=True)

            file1 = open('conf/configuration_ubuntu_setup2.txt', 'r')
            Lines = file1.readlines()
            file1.close()

            for line in Lines:
                a1 = "{}".format(line.strip())
                subprocess.call(""+ a1 +"", shell=True, universal_newlines=True)
            subprocess.call("rm -rf conf/configuration_ubuntu_setup2.txt", shell=True, universal_newlines=True)


        else:
            print("You've AWS CLI installed")

        print("============================================================================================")
        print("Checking for Ansible packages")
        countdown(2)
        ansible_package = subprocess.check_output("apt list --installed ansible 2>/dev/null | grep -i installed | wc -l", shell=True, universal_newlines=True)

        if (int(ansible_package.strip()) == 0):
            print("Package Ansible isn't installed, installing it now !!!")
            countdown(2)
            subprocess.check_output("grep -A 2 ansible_package conf/configuration_ubuntu_setup.txt | grep -v ansible_package > conf/configuration_ubuntu_setup3.txt", shell=True, universal_newlines=True)

            file1 = open('conf/configuration_ubuntu_setup3.txt', 'r')
            Lines = file1.readlines()
            file1.close()

            for line in Lines:
                a1 = "{}".format(line.strip())
                subprocess.call(""+ a1 +"", shell=True, universal_newlines=True)
            subprocess.call("rm -rf conf/configuration_ubuntu_setup3.txt", shell=True, universal_newlines=True)

        else:
            print("You've Ansible installed")
        print("============================================================================================")

        print("Initializing Terraform Repo.")
        subprocess.call("cd terraform; terraform init", shell=True, universal_newlines=True)


        print("============================================================================================")
        print("************************************Setup Completed*****************************************")

def startdp():
    print("************************************Starting Installation*****************************************")
    print("==================================================================================================")

    ak = input("Enter your AWS IAM Access Key\n")
    sk = input("Enter your AWS IAM Secret Key\n")
    countdown(2)

    subprocess.call("cp terraform/terraform.tfvars_orig terraform/terraform.tfvars", shell=True, universal_newlines=True)
    subprocess.call("sed -i 's/ACCESS_KEY/"+ ak +"/g' terraform/terraform.tfvars", shell=True, universal_newlines=True)
    subprocess.call("sed -i 's/SECRET_KEY/"+ sk +"/g' terraform/terraform.tfvars", shell=True, universal_newlines=True)
    print("==================================================================================================")

    print("Launching the AWS VMs")
    countdown(2)
    subprocess.call("cd terraform; terraform apply -auto-approve", shell=True, universal_newlines=True)
    print("==================================================================================================")

    print("Setting up AWS CLI")
    countdown(2)
    subprocess.call("aws configure", shell=True, universal_newlines=True)
    print("==================================================================================================")

    # PUB IP starts here
    print("Creating Ansible host file from default")
    countdown(2)
    subprocess.check_output("cp ansible/inventory/default_hosts ansible/inventory/hosts", shell=True)

    jenkinsserverPuIP = subprocess.check_output("aws ec2 describe-instances  --query \"Reservations[*].Instances[*].{PublicIP:PublicIpAddress}\" --filter \"Name=tag:Name,Values='jenkinsserver'\"", shell=True, universal_newlines=True)
    s0 = re.compile(r"\d+[.]\d+[.]\d+[.]\d+")
    m0 = s0.search(jenkinsserverPuIP)
    PuIPj = m0.group(0)

    print("Public IP of Jenkins Server",PuIPj)
    subprocess.check_output("sed -i 's/PuIPj/"+ PuIPj +"/g' ansible/inventory/hosts", shell=True, universal_newlines=True)

    sonarqubeserverPuIP = subprocess.check_output("aws ec2 describe-instances  --query \"Reservations[*].Instances[*].{PublicIP:PublicIpAddress}\" --filter \"Name=tag:Name,Values='sonarqubeserver'\"", shell=True, universal_newlines=True)
    s0 = re.compile(r"\d+[.]\d+[.]\d+[.]\d+")
    m0 = s0.search(sonarqubeserverPuIP)
    PuIPs = m0.group(0)

    print("Public IP of Sonarqube Server",PuIPs)
    subprocess.check_output("sed -i 's/PuIPs/"+ PuIPs +"/g' ansible/inventory/hosts", shell=True, universal_newlines=True)


    webserverPuIP = subprocess.check_output("aws ec2 describe-instances  --query \"Reservations[*].Instances[*].{PublicIP:PublicIpAddress}\" --filter \"Name=tag:Name,Values='webserver'\"", shell=True, universal_newlines=True)
    s0 = re.compile(r"\d+[.]\d+[.]\d+[.]\d+")
    m0 = s0.search(webserverPuIP)
    PuIPw = m0.group(0)

    print("Public IP of Webserver",PuIPw)
    subprocess.check_output("sed -i 's/PuIPw/"+ PuIPw +"/g' ansible/inventory/hosts", shell=True, universal_newlines=True)

    elkserverPuIP = subprocess.check_output("aws ec2 describe-instances  --query \"Reservations[*].Instances[*].{PublicIP:PublicIpAddress}\" --filter \"Name=tag:Name,Values='elkserver'\"", shell=True, universal_newlines=True)
    s0 = re.compile(r"\d+[.]\d+[.]\d+[.]\d+")
    m0 = s0.search(elkserverPuIP)
    PuIPelk = m0.group(0)

    print("Public IP of ELK Server",PuIPelk)
    a = subprocess.check_output("sed -i 's/PuIPelk/"+ PuIPelk +"/g' ansible/inventory/hosts", shell=True, universal_newlines=True)

    pgaserverPuIP = subprocess.check_output("aws ec2 describe-instances  --query \"Reservations[*].Instances[*].{PublicIP:PublicIpAddress}\" --filter \"Name=tag:Name,Values='pgaserver'\"", shell=True, universal_newlines=True)
    s0 = re.compile(r"\d+[.]\d+[.]\d+[.]\d+")
    m0 = s0.search(pgaserverPuIP)
    PuIPpga = m0.group(0)

    print("Public IP of PGA server",PuIPpga)
    a = subprocess.check_output("sed -i 's/PuIPpga/"+ PuIPpga +"/g' ansible/inventory/hosts", shell=True, universal_newlines=True)

    final = subprocess.check_output("aws ec2 describe-instances  --query \"Reservations[*].Instances[*].{PublicIP:PublicIpAddress,PrivateIP:PrivateIpAddress,Name:Tags[?Key=='Name']|[0].Value,Status:State.Name}\" --filter \"Name=tag:Name,Values='jenkinsserver','sonarqubeserver','webserver','elkserver','pgaserver'\" \"Name=instance-state-name,Values=running\" --output table", shell=True, universal_newlines=True)
    print(final)
    print("==================================================================================================")

    print("Inserting Public and Private IP address of AWS VMs to the config files")
    countdown(2)

    subprocess.check_output("cp ansible/jenkins/conf/jenkinsnginx_nginx.conf_orig ansible/jenkins/conf/jenkinsnginx_nginx.conf", shell=True)
    subprocess.check_output("sed -i 's/PuIPj/"+ PuIPj +"/g' ansible/jenkins/conf/jenkinsnginx_nginx.conf", shell=True, universal_newlines=True)

    subprocess.check_output("cp ansible/sonarqube/conf/sonarqubenginx_nginx.conf_orig ansible/sonarqube/conf/sonarqubenginx_nginx.conf", shell=True)
    subprocess.check_output("sed -i 's/PuIPs/"+ PuIPs +"/g' ansible/sonarqube/conf/sonarqubenginx_nginx.conf", shell=True, universal_newlines=True)

    subprocess.check_output("cp ansible/webserver/conf/learneasyai_nginx.conf_orig ansible/webserver/conf/learneasyai_nginx.conf", shell=True)
    subprocess.check_output("sed -i 's/PubIPw/"+ PuIPw +"/g' ansible/webserver/conf/learneasyai_nginx.conf", shell=True, universal_newlines=True)
    
    subprocess.check_output("cp ansible/elk_stack/elkserver.yml_orig ansible/elk_stack/elkserver.yml", shell=True, universal_newlines=True)
    subprocess.check_output("cp ansible/elk_stack/conf/filebeat.yml_orig ansible/elk_stack/conf/filebeat.yml", shell=True, universal_newlines=True)
    
    elkserverPrIP = subprocess.check_output("aws ec2 describe-instances  --query \"Reservations[*].Instances[*].{PrivateIP:PrivateIpAddress}\" --filter \"Name=tag:Name,Values='elkserver'\"", shell=True, universal_newlines=True)
    s0 = re.compile(r"\d+[.]\d+[.]\d+[.]\d+")
    m0 = s0.search(elkserverPrIP)
    PrIPelk = m0.group(0)

    subprocess.check_output("sed -i 's/PrIPelk/"+ PrIPelk +"/g' ansible/elk_stack/elkserver.yml", shell=True, universal_newlines=True)
    subprocess.check_output("sed -i 's/PrIPelk/"+ PrIPelk +"/g' ansible/elk_stack/conf/filebeat.yml", shell=True, universal_newlines=True)

    subprocess.check_output("cp ansible/prometheus_stack/conf/node-rules.yml_orig ansible/prometheus_stack/conf/node-rules.yml", shell=True, universal_newlines=True)
    subprocess.check_output("cp ansible/prometheus_stack/conf/prometheus.yml_orig ansible/prometheus_stack/conf/prometheus.yml", shell=True, universal_newlines=True)
    
    webserverPrIP = subprocess.check_output("aws ec2 describe-instances  --query \"Reservations[*].Instances[*].{PrivateIP:PrivateIpAddress}\" --filter \"Name=tag:Name,Values='webserver'\"", shell=True, universal_newlines=True)
    s0 = re.compile(r"\d+[.]\d+[.]\d+[.]\d+")
    m0 = s0.search(webserverPrIP)
    PrIPw = m0.group(0)

    subprocess.check_output("sed -i 's/PrIPw/"+ PrIPw +"/g' ansible/prometheus_stack/conf/node-rules.yml", shell=True, universal_newlines=True)
    subprocess.check_output("sed -i 's/PrIPw/"+ PrIPw +"/g' ansible/prometheus_stack/conf/prometheus.yml", shell=True, universal_newlines=True)

    print("==================================================================================================")

    print("Installing Jenkins on Jenkinsserver using Ansible")
    countdown(2)
    subprocess.call("cd ansible/jenkins; ansible-playbook jenkins.yml -i ../inventory/hosts", shell=True, universal_newlines=True)
    print("==================================================================================================")

    print("Installing Sonarqube on sonarqubeserver using Ansible")
    countdown(2)
    subprocess.call("cd ansible/sonarqube; ansible-playbook sonarqube.yml -i ../inventory/hosts", shell=True, universal_newlines=True)
    print("==================================================================================================")

    print("Installing Sonarqube Scanner on jenkins server using Ansible")
    countdown(2)
    subprocess.call("cd ansible/sonarqube; ansible-playbook sonarqube_scanner.yml -i ../inventory/hosts", shell=True, universal_newlines=True)
    print("==================================================================================================")

    print("Installing Django app,gunicorn, nginx on webserver using Ansible")
    countdown(2)
    subprocess.call("cd ansible/webserver; ansible-playbook django.yml -i ../inventory/hosts", shell=True, universal_newlines=True)
    print("==================================================================================================")

    print("Installing ELK stack using Ansible")
    countdown(2)
    subprocess.call("cd ansible/elk_stack; ansible-playbook elkserver.yml -i ../inventory/hosts", shell=True, universal_newlines=True)
    print("==================================================================================================")

    print("Installing Prometheus, Grafana, Node-Exporter and Alertmanager using Ansible")
    countdown(2)
    subprocess.call("cd ansible/prometheus_stack; ansible-playbook pgaserver.yml -i ../inventory/hosts", shell=True, universal_newlines=True)
    print("==================================================================================================")

    print("Getting the Jenkins initial Admin Password")
    countdown(3)
    jen_pass = subprocess.check_output("ssh -i terraform/id_rsa ubuntu@"+ PuIPj +" sudo cat /var/lib/jenkins/secrets/initialAdminPassword", shell=True, universal_newlines=True)
    print("Login to Jenkins web GUI and paste this password => ",jen_pass)
    print("==================================================================================================")

    print("************************************Installation Completed*****************************************") 

def stopdp():
    
    print("************************************Destruction Started********************************************")
    print("==================================================================================================")
    print("Destroying the AWS server")
    countdown(2)
    subprocess.call("cd terraform; terraform destroy -auto-approve", shell=True, universal_newlines=True)
    print("==================================================================================================")

    print("Removing files")
    countdown(2)
    subprocess.check_output("rm -rf ansible/inventory/hosts", shell=True, universal_newlines=True)
    subprocess.check_output("rm -rf ansible/webserver/conf/learneasyai_nginx.conf", shell=True, universal_newlines=True)
    subprocess.check_output("rm -rf ansible/elk_stack/elkserver.yml", shell=True, universal_newlines=True)
    subprocess.check_output("rm -rf ansible/elk_stack/conf/filebeat.yml", shell=True, universal_newlines=True)
    subprocess.check_output("rm -rf ansible/prometheus_stack/conf/node-rules.yml", shell=True, universal_newlines=True)
    subprocess.check_output("rm -rf ansible/prometheus_stack/conf/prometheus.yml", shell=True, universal_newlines=True)
    subprocess.check_output("rm -rf ansible/sonarqube/conf/sonarqubenginx_nginx.conf", shell=True, universal_newlines=True)
    subprocess.check_output("rm -rf ansible/jenkins/conf/jenkinsnginx_nginx.conf", shell=True, universal_newlines=True)
    subprocess.check_output("rm -rf rm -rf terraform/terraform.tfvars", shell=True, universal_newlines=True)
    subprocess.check_output("rm -rf terraform/terraform.tfstate", shell=True, universal_newlines=True)
    subprocess.check_output("rm -rf terraform/terraform.tfstate.backup", shell=True, universal_newlines=True)
    subprocess.check_output("rm -rf terraform/.terraform*", shell=True, universal_newlines=True)
    subprocess.check_output("rm -rf terraform/id_rsa*", shell=True, universal_newlines=True)
    print("==================================================================================================")
    print("************************************Destruction Completed********************************************")

if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-A', '--action', dest='action', help='Action you would like to do', type=str)
    args = parser.parse_args()

    action = args.action

    if (action=='setup'):
       setupdp()
    elif (action=='start'):
        startdp()
    elif (action=='stop'):
        stopdp()
    elif (action=='status'):
        statusad()
    elif (action=='port_check'):
        port_check()
    elif (action=='hard_stop'):
        hardstopad()
    elif (action=='conn_check'):
        conncheck()
    else:
        print("python3 automation_devops.py -A <setup|start|stop|hard_stop|status|port_check|conn_check>")


